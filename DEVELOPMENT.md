# Week One

### 18.04.20

For whatever reason, Git does not want to let me connect my local copy of my work with GitHub- so I guess I will have to patch my code manually on GitHub.com, which isn't that bad. Two hectic days of refamiliarizing myself with the "Processing" design strategy- except this time it's for C++ instead of pseudoJava. I have laid out almost the entire backbone of my project in comments and have created a proof of concept main() that displays a static "example" visualizer- as well as a prompt for a user. Not enough time to figure out how to get file opening or FMOD audio playing working perfectly. Those will have to come later. For now, the internal structure of the project looks good.

What remains:
1. Implementing Fast Fourier Transform, whether that is through FMOD or OpenFrameWorks natively.
2. Create abstract class for visualizers to fit in- likely located outside of any of the CPP files I've written so far.
3. Write visualizers - arguably going to take the least amount of time, if only because their implementation is straightforward & only requires creativity.

Because of the previously mentioned problems with GitHub, my 'bin' folder does not want to upload properly, so I have only included a copy of the debugging exe instead of the usual output. Will make sure folder hierarchy is intact next time.

## Week Two

### 18.04.23

Implementing FFT analysis turns out to be far easier to accomplish with native OpenFrameworks library methods than with FMOD. I will need to seek out a new library to fully satisfy the final project rubric. It will probably be OpenGL or another graphics library. For visualization testing / adjustments, I've been using an mp3 version of "The Honeydripper" by the Oscar Peterson Trio, but I'm not getting much of a response for higher frequencies, while lower frequencies have a lot of dynamic range. Will have to experiment with taking more frequency samples from my audio, and trying with more songs with different accoustic signatures. 

### 18.04.24

Implemented some basic controls to control the audio- spacebar to pause/play, 'o' to stop the music and open a new track, 'p' to stop music playback entirely. Pausing includes a fade-in/fade-out effect which turns out to be more difficult than expected to implement- since openframe. My main objective next is to create an abstract "visualizer" class that I can use to create different visualizers from- and organize very simply into my main app. 

### 18.04.29

Finally figured out my optimal FFT settings. I ended up writing a math expression that took me a while to figure out, but now I can safely say that my program displays a WEIGHTED analysis of the audio, instead of a direct/linear one. By this, I mean that I was able to find a math expression that makes sure that there is less contrast between the peaks/troughs of the FFT data, while also pronouncing the relative highs of each frequency range. I consider this to be a major accomplishment for my program, as an example of applied mathematics to achieve some sort of visually appealing AND functional solution to a problem. I will include this expression in my README file.

## Week Three

### 18.04.30

Figured out one of my biggest bugs! Visualizer data was not being overridden for low new updates in intensity. Values would be stuck at their highest peaks, only changing when they increased to adjust to new peaks in volume. I thought that this was a floating point rounding error- values not smoothing to zero properly because multiplying them by 0.98 would have no effect for larger precise numbers. It turns out that this was an error in logic, where I had no case that handled old values being greater than new values. This was due to storing my values in their own variables instead of comparing a new variable to a value in the data array. Thank you office hours. Hours of staring at the same code can make you assume how some things work.

### 18.05.01

Wrote & implemented a whole new visualizer based off of the Unknown Pleasures album cover, and made my project entirely dependent on classes which implement an abstract class in order to render anything on screen. Moving methods from my ofApp.cpp file to their own implementsAbstractVisualizer.h files was time consuming and required turning a lot of regular variables into member variables, but now those two visualizers are nicely set apart & very easy to read. I hope this shows.

### 18.05.02

Since I think the math that went into this project is the most interesting part about it, I added some sliders in the bottom left corner so that the user can interact with the math and find the settings that they like the most. One new visualizer was added, which graphs the frequency data to polar coordinates instead of on a cartesian plane. Like the Joy Division visualizer, this one does an interesting job of bringing out eccentricies in certain frequencies. I also improved the look of my default visualizer so that it now rests inside of a box in the middle of the screen, and the vertical bars move upward instead of downward. 

Now that the user can:
- Open their own sound file
- Adjust the parameters of the fft-smoothing equation (using **ofxGUI**)
- Pause, play, and stop the sound
- Switch between visualizers
- And, most importantly, SEE the graphic interpretation of the sound that they're playing

My project is now feature complete.
