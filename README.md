# CS 126 Final Project: Audio Visualizer

Created by David Mitchell, UIUC '21. https://astrosticks.com

***

## About

This is a project created for CS 126 at UIUC. This program allows the user to do two things:
1. View a sound file of their choice through a "visualizer"- graphics which change according to differences in the sound as it's playing.
2. See how changing parameters can change the output of a visualizer.

Watch a demonstration:  
<a href="https://www.youtube.com/watch?v=2Kt8-onxEtI">
<img src="https://i.ytimg.com/vi/2Kt8-onxEtI/maxresdefault.jpg" width="350" title="YouTube Link">
</a>

### Features

- Switch between three unique audio visualizations
- Play any standard audio file (.mp3, .wav, .flac)
- Controls for pause / play / stop
- Adjust audio analysis parameters- find the setting that you like most


***


Build requirements:

**Windows Vista, 7, 8, 10** (Due to system dialogue settings)
**MS Visual Studio 2017+** - https://www.visualstudio.com/  
**OpenFrameworks** - http://openframeworks.cc/  
**ofxGUI** - http://openframeworks.cc/documentation/ofxGui/  


***


### Special thanks to...
Katy - https://katyscode.wordpress.com  
ofBook contributors - http://openframeworks.cc/ofBook/chapters/foreword.html  
Professor Evans - https://cs.illinois.edu/directory/profile/gcevans  
