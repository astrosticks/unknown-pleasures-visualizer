# Project Proposal: Audio Visualizer Kit

An application made with **OpenFrameworks** which allows the user to play any standard audio file on their system (.mp3, .wav, .flac, etc.) and see real-time motion graphics calculated with information decoded from the sound. I will be using **ofxGUI** to create a custom interface to interact with the program by changing its parameters. I will create three or four different "visualization" classes, each inheriting from the same abstract class, so that my code is modular and fast.

I will be using **OpenFrameworks** to both play and analyze audio, since it comes with a highly configurable method of performing FFT on a soundwave. FFT stands for Fast Fourier Transform (https://en.wikipedia.org/wiki/Fast_Fourier_transform), and is a mathematical function used to convert an intensity vs time graph (a regular soundwave) into an intensity vs frequency graph. This will be the one of the cornerstone pieces of my program, since FFT will allow me to dissect the frequency graph of the current audio and change the on-screen graphics accordingly. FFT is the basis for discovering pitch, key, volume, and beat in musical audio analysis.

### ofxGUI

I originally had proposed to use one library, FMOD, and when I found out that that was technically already part of OpenFrameworks, I decided to use ofxFX instead. Unfortunately, even after going to office hours, I wasn't able to even make the example projects for ofxFX compile. So I instead decided to use ofxGUI. What I learned during this assignment was how interesting programs can become when their functionality comes from mathmatical formulas. Being able to tweak the parameters in my code made my project behave completely differently, so in addition to the purpose of my project as described earlier, my project now is also a way to explore how different parameters can effect the output of an audio analysis algorithm. There are two number sliders, and adjusting them can change significant portions of the appearance of the visualizers.

### Background

I love audio visualizers, and can remember playing with them since before I was into music! Unfortunately, my background in them ends at the aesthetic investigation. OpenFrameworks is ubiquitous enough that I'm very interested to see how others have implemented similar ideas before, and how I can expand/alter/diverge from them! My eye for graphic design will also ensure that my final project is a visually interesting experience for the average user.
