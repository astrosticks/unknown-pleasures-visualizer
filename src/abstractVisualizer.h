#ifndef _OFAPPVIZU
#define _OFAPPVIZU

#include "ofApp.h"

/*
An abstract C++ class, representing an interface to a "visualizer".
Models functions which the main application can call to an ofAppVizu
pointer, regardless of what type of visualizer is actually pointed to.
*/
class ofAppVizu {

public:

	/*
	Returns a written description of the visualizer, in the form
	of a vector of strings.
	*/
	virtual std::vector<std::string> getDescription() = 0;

	/*
	Runs once, before update() or draw() are called. A place to
	initialize values / compute things once.
	*/
	virtual void setup() = 0;

	/*
	Runs once every tick, before draw(). Good place to do calculations
	to prepare for drawing, or updating parameters based on outside
	conditions. (window size changed, etc.)
	*/
	virtual void update() = 0;

	/*
	Runs once every tick, after update(). This is where the majority
	of graphics rendering will take place, based of parameters set in
	update().
	*/
	virtual void draw(std::array<double, 128> &fft_data) = 0;
};
#endif