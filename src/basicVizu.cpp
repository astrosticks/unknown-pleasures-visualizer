#ifndef _OFAPPBASICV
#define _OFAPPBASICV

#include "ofAppVizu.h"

class basicVizu : public ofAppVizu {

	const int kAmp = 100;
	const int kRes = 128;
	const int kGap = 1;

public:

	// CONSTRUCTOR

	basicVizu(std::array<float, 128>* parent_spect) {
		fft_spect_ = parent_spect;
	}

	// --- METHODS ---

	// --------------------------
	void setup() 
	{
		ofBackground(66, 134, 244);
	}

	// --------------------------
	void update()
	{
		return;
	}
	
	// --------------------------
	void draw()
	{
		// drawn in white
		ofSetColor(255, 255, 255);

		// draw a 1px tall rectangle at the maximum 
		ofDrawRectangle(0, kAmp + 1, ofGetViewportWidth(), 1);

		// draws 'plot_resolution_' number of vertical bars next to eachother
		for (int i = 0; i < kRes; i++)
		{
			// calculate horizontal components of bar
			int width = (ofGetWindowWidth() / kRes);	// the width of a bar minus 
			int left = i * width;							// the index of the bar times the width of a bar

			// calculate vertical components of bar
			int top = 0;	// the height of the bar
			int height = i;	// the distance from 'top' to the bottom of the window

			ofDrawRectangle(
				left,	// x coordinate of left edge
				top,	// y coordinate of top edge
				width - kGap,	// rendered width of object (places a gap between each bar)
				height			// rendered height of object
			);
		}
	}
};
#endif