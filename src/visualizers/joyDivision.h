#ifndef _OFAPPJOYD
#define _OFAPPJOYD

#include "abstractVisualizer.h"

/*
Draws a visualizer which resembles the album art of Unknown Pleasures by Joy Division (1979).
Each horizontal line drawn is referred to in code as a "graph."
Graphs are varied from the data using a noise calculation with the vertical index
of the graph as a seed.
*/
class joyDivision : public ofAppVizu {

public:

	std::vector<std::string> getDescription() {
		// ---------------------------------------------|
		std::vector<std::string> description = {
			"Based on the album cover for Unknown Pleasures by",
			"Joy Division, released in 1979. Each horizontal line",
			"is a graph of a subsection of the entire frequency",
			"range. Points are hightened in the center by mapping",
			"their index to a parabola of height one, taken to an",
			"arbitrary power greater than one."
		};
		// ---------------------------------------------|
		return description;
	}

	// --- CUSTOM PARAMETERS ---

	// Define the number of graphs to draw & 
	// space evenly on the screen.
	const int kNumOfGraphs = 60;

	// Define the number of data points plotted per graph.
	const int kVerticesPerGraph = 65;

	// Define width of line drawn ontop of graph.
	const double kLineWinRatioWidth = 0.008;

	// Define the ratio of a graph's amplitude to its width.
	const double kGraphAmpWidthRatio = 0.25;

	// Rules for scaling:
	// VizuHeight / WindowHeight ratio will be at most 3/5.
	// - VizuWidth / WindowWidth ratio will be at most 1/2.
	// - - VizuWidth / VizuHeight ratio will ALWAYS be 4/5.
	const double kVizuWinMaxRatioHeight = 0.6;
	const double kVizuWinMaxRatioWidth = 0.5;
	const double kVizuRatioWidthHeight = 0.8;

	// Define a power to raise the quadratic result from 
	// getQuadraticSpectAdjust() to.
	const double kQuadPostPower = 7.0;


	// --- UNINITIALIZED VARIABLES ---

	// Calculate and store the height of a graph.
	double graph_amp_;

	// Calculate spatial boundaries to draw vizualizer in.
	int vizu_left_bound_, vizu_top_bound_;
	int	vizu_width_, vizu_height_;

	// Calculate & store the vertical distance between each 
	// graph's baseline.
	double graph_vert_gap_;

	// Calculate & store the horizontal distance between the vertices
	// of a graph.
	double graph_horz_gap_;

	// Calculate & store the width of the lines drawn on the graphs.
	double graph_line_width_;


	// --- METHODS ---

	// --------------------------
	void setup() 
	{
		// Set background to a dark grey.
		ofBackground(10, 10, 10);

		// Enable anti-aliasing.
		ofEnableSmoothing();
	}

	// --------------------------
	void update()
	{
		// -- Adjust dimensions of visualizer depending on window size --

		// If window is taller than it is wide,
		if (ofGetWindowHeight() > ofGetWindowWidth()) {

			// Calculate width (constrained) ----------------------------------

				// Set visualizer width based on VizuWidth / WindowWidth ratio.
				vizu_width_ = kVizuWinMaxRatioWidth * ofGetWindowWidth();

				// Set distance from visualizer to left side of 
				// screen so that visualizer is centered horizontally.
				vizu_left_bound_ =
						(ofGetWindowWidth() / 2.0)
					-	(vizu_width_ / 2.0);

			// Calculate height (based on width) ------------------------------

				// Set visualizer height based on width / height ratio.
				vizu_height_ = (1.0 / kVizuRatioWidthHeight) * vizu_width_;

				// Set distance from visualizer to top side of 
				// screen so that visualizer is centered vertically.
				vizu_top_bound_ =
						(ofGetWindowHeight() / 2.0)
					-	(vizu_height_ / 2.0);
		}
		// If window is wider than it is tall,
		else {

			// Calculate height (constrained) ---------------------------------

				// Set visualizer height based on VizuHeight / WindowHeight ratio.
				vizu_height_ = kVizuWinMaxRatioHeight * ofGetWindowHeight();

				// Set distance from visualizer to top side of
				// screen so that visualizer is centered vertically.
				vizu_top_bound_ =
						(ofGetWindowHeight() / 2.0)
					-	(vizu_height_ / 2.0);

			// Calculate width (based on height) ------------------------------

				// Set visualizer width based on width / height ratio.
				vizu_width_ = kVizuRatioWidthHeight * vizu_height_;

				// Set distance from visualizer to left side of 
				// screen so that visualizer is centered horizontally.
				vizu_left_bound_ =
						(ofGetWindowWidth() - vizu_width_)
					/	2.0;
		}

		// Define the distance between each graph's baseline.
		// (based on height)
		graph_vert_gap_ = (double)vizu_height_ / kNumOfGraphs;

		// Define the horizontal distance between vertices of graphs.
		// (based on width)
		graph_horz_gap_ = (double)vizu_width_ / kVerticesPerGraph;

		// Define the maximum height of a graph. 
		// (based on width)
		graph_amp_ = vizu_width_ * kGraphAmpWidthRatio;

		// Define the width of the lines to be rendered.
		// (based on width)
		graph_line_width_ = vizu_width_ * kLineWinRatioWidth;
	}
	
	// --------------------------
	
	void draw(std::array<double, 128> &fft_data)
	{
		srand(0);

		// Draw kNumOfGraphs of graphs.
		for (int i = 0; i < kNumOfGraphs; i++)
		{
			// Create ofPolyline object to be top line of graph.
			ofPolyline* current_graph = new ofPolyline();

			// Define the default y-coordinate of a vector in current_graph.
			double graph_base_y = vizu_top_bound_ + (i * graph_vert_gap_);

			// Get a random number to offset each index of a graph.
			int idx_offset = rand() % ofApp::kVizuResolution;

			// Get a random number to multiply the amplitude of each graph by.
			double rand_amp_factor = 0.5 + ((double) rand() / (RAND_MAX*2.0));

			for (int j = 0; j < kVerticesPerGraph; j++) {

				int data_index = j + idx_offset;

				double vert_rel_x =
						j					// The index of this vertex (from left to right).
					*	graph_horz_gap_;	// The horizontal distance between two vertices.

				double vert_rel_y =
					// The value of the FFT data array, at an index j + some offset,
					// wrapped around to within the range of fft_data.
						fft_data[data_index % ofApp::kVizuResolution]	
					// A value which curves the graph around the center.
					// Vertices in the middle will be tallest, ones on 
					// the sides will be shortest.
					*	getQuadraticSpectAdjust(j)
					// The maximum amplitude of the graph (the only value greater than 1 here).
					*	graph_amp_
					// A random height factor.
					*	rand_amp_factor;

				current_graph->addVertex(
					ofPoint(
						vizu_left_bound_ + vert_rel_x,	// Rendered x position
						graph_base_y - vert_rel_y		// Rendered y position
					)
				);
			}
			// Fill area with dark grey.
			ofSetColor(10, 10, 10);
			ofApp::fillPolyline(current_graph);
			
			// Draw line on top of graph.
			ofSetColor(245, 245, 245);
			ofApp::tracePolyline(current_graph, graph_line_width_);

			delete current_graph;
		}
	}

	/*
	Calculates a value from 0.0 - 1.0, according to where input lies on
	a parabola of height 1 width a base length of kVizuResolution.

	Returns this value to a specified power (to pronounce the values
	in the middle of the data).
	*/
	double getQuadraticSpectAdjust(int x) {

		int m = kVerticesPerGraph - 1;

		// If input lies out of range, assume that these values are
		// supposed to return zero (so that the function is
		// somewhat continuous).
		if (x < 0 || x > m) {
			return 0.0;
		}
		// Do a transformation on the input based on the 
		// quadratic formula... this maps the input to a
		// parabola which intersects the x-axis at 0 and
		// (kVizuResolution - 1) with a maximum value at 
		// (kVizuResolution / 2) of 1.
		double quadraticResult =
			(4 * x * (m - x))
			/ (pow(m, 2));

		// Raise the result to kQuadPostPower, to control the range
		// of the most pronounced frequencies.
		return pow(quadraticResult, kQuadPostPower);
	}
};
#endif