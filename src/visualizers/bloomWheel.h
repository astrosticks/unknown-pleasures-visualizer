#ifndef _OFAPPBLOOMWHEEL
#define _OFAPPBLOOMWHEEL

#include "abstractVisualizer.h"

/*
Draws three concentric wheels, whose borders morph according to the
data in a particular subsection of all of the frequencies.
*/
class bloomWheel : public ofAppVizu {

public:

	std::vector<std::string> getDescription() {
		// ---------------------------------------------|
		std::vector<std::string> description = {
			"Draws three concentric wheels, whose borders",
			"morph according to the data from different",
			"subsections of all of the frequencies."
		};
		// ---------------------------------------------|
		return description;
	}

	// --- CUSTOM PARAMETERS ---

	// Define ratio of visualizer radius to smallest window dimmension.
	const double kRadiusWinRatio = 0.1;

	// Define ratio of vizualizer maximum amplitude to radius.
	const double kAmpRadiusRatio = 1.5;

	// Define the number of frequencies to be displayed by each wheel.
	int steps_ = 32;


	// --- UNINITIALIZED VARIABLES ---

	// Calculate and store the radius of the visualizer.
	double vizu_radius_;

	// Calculate and store the max height of the visualizer.
	double vizu_amp_;

	// Store screen dimensions
	int width_, height_;

	// --- METHODS ---

	// --------------------------
	void setup() 
	{
		// Set background to black.
		ofBackground(ofColor::white);
	}

	// --------------------------
	void update()
	{
		// Get the smaller of the two dimensions: Window height or width.
		int smallest_window_dimension = min(
			ofGetWindowWidth(),
			ofGetWindowHeight()
			);

		// Calculate the new visualizer radius.
		// (based on smallest_win_dim)
		vizu_radius_ = smallest_window_dimension * kRadiusWinRatio;

		// Calculate the maximum displacement of the visualizer from its radius.
		// (based on radius).
		vizu_amp_ = vizu_radius_ * kAmpRadiusRatio;
	}
	
	// --------------------------
	void draw(std::array<double, 128> &fft_data)
	{
		// Draw three different wheels.
		for (int g = 0; g < 3; g++) {
		
			// Create a new wheel, with a sort-of random angle offset.
			ofPolyline wheel;
			double angle_offset = (4 - g)*(TWO_PI / 7.0);
			// Create connected vertices around the center, in such a way that
			// the first wheel is the largest, and the last wheel is the smallest.
			for (int i = 0; i < steps_; i++) {

				// Calculate polar coordinates of vertex.
				// Angle is determined as a linear division around a whole circle.
				double angle = (i * (TWO_PI) / (steps_)) + angle_offset;
				// Magnitude is determined by visualization data and by which circle is being drawn.
				double magnitude = 0.4*vizu_radius_*(4-g) + (vizu_amp_*fft_data[ofApp::kVizuResolution - (((3-g))*steps_) + i]);

				// Convert from polar coordinates to cartesian.
				int pointX = (ofGetWindowWidth()/2.0) + (double)(magnitude*cos(angle));
				int pointY = (ofGetWindowHeight()/2.0) + (double)(magnitude*sin(angle));

				wheel.addVertex(pointX, pointY);
			}
			wheel.addVertex(wheel.getVertices().at(0));
			// Pick wheel color based on evenness of wheel index.
			if ((g+1) % 2) {
				ofSetColor(ofColor::green);
			}
			else 
			{
				ofSetColor(ofColor::yellow);
			}
			// Fill the object with color.
			ofApp::fillPolyline(&wheel);

		}

	}
};
#endif