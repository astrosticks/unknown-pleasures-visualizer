#ifndef _OFAPPBASICV
#define _OFAPPBASICV

#include "ofApp.h"
#include "abstractVisualizer.h"

class basicVizu : public ofAppVizu 
{
	const int kAmp = 384;
	const int kRes = 128;
	const int kWidth = 3;
	const int kGap = 1;

	const int kSquarePad = 20;

	int left_offset_;

public:

	std::vector<std::string> getDescription() {
		// ---------------------------------------------|
		std::vector<std::string> description = {

			"A basic visualizer made to display the app's",
			"raw data from the fft + smooth calculations.",
			"OpenFrameworks documentation is actually",
			"wrong about sound spectrum analysis, so",
			"lower frequencies clip, but are constrained",
			"to the rendered box."
		};
		// ---------------------------------------------|
		return description;
	}

	// --------------------------
	void setup() 
	{
		ofBackground(66, 134, 244);
	}

	// --------------------------
	void update()
	{
		left_offset_ = (ofGetWindowWidth() - (kWidth*ofApp::kVizuResolution)) / 2;
	}
	
	// --------------------------
	
	void draw(std::array<double, 128> &fft_data)
	{
		
		// drawn in white
		ofSetColor(255, 255, 255);

		// Draw rectangle around entire visualizer.
		ofDrawRectangle( // Left border
			left_offset_ - kSquarePad,
			ofGetWindowHeight()*(4.0 / 5.0) - kAmp - kSquarePad,
			1,
			kAmp + 2*kSquarePad
			);
		ofDrawRectangle( // Right border
			left_offset_ + kSquarePad + ofApp::kVizuResolution*kWidth,
			ofGetWindowHeight()*(4.0 / 5.0) - kAmp - kSquarePad,
			1,
			kAmp + 2 * kSquarePad
		);
		ofDrawRectangle( // Bottom border
			left_offset_ - kSquarePad,
			ofGetWindowHeight()*(4.0 / 5.0) - kAmp - kSquarePad + kAmp + 2*kSquarePad,
			2 * kSquarePad + ofApp::kVizuResolution*kWidth,
			1
		);
		ofDrawRectangle( // Top border
			left_offset_ - kSquarePad,
			ofGetWindowHeight()*(4.0 / 5.0) - kAmp - kSquarePad,
			2 * kSquarePad + ofApp::kVizuResolution*kWidth,
			1
		);

		// draws 'plot_resolution_' number of vertical bars next to eachother
		for (int i = 0; i < kRes; i++)
		{
			// calculate horizontal components of bar
			int width = kWidth;		// the width of a bar minus 
			int left = i * width;	// the index of the bar times the width of a bar

			// calculate vertical components of bar
			int top = 0;					// the height of the bar
			int height = fft_data[i]*kAmp;	// the distance from 'top' to the bottom of the window
			height = std::min(height, kAmp);

			ofDrawRectangle(
				left_offset_ + left,	// x coordinate of left edge
				ofGetWindowHeight()*(4.0/5.0) - height,					// y coordinate of top edge
				width - kGap,			// rendered width of object (places a gap between each bar)
				height					// rendered height of object
			);
		}
		
	}
};
#endif