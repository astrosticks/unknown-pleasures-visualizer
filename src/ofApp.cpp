// Include openframeworks libraries & addons
#include "ofApp.h"
// Include classes which implement abstractVisualizer
#include "visualizers/simple.h"
#include "visualizers/joyDivision.h"
#include "visualizers/bloomWheel.h"


// ----------------- OpenFrameWorks methods --------------------

//--------------------------------------------------------------
void ofApp::setup(){

	// Hide the console.
	ShowWindow(GetConsoleWindow(), SW_HIDE);

	// Initialize GUI.
	sliders_.setup();
	sliders_.add(idx_adjust_power_.setup("Index power: ", 12.0, 1.0, 20.0));
	sliders_.add(val_adjust_power_.setup("Value power: ", 0.35, 0.1, 1.5));
	
	// Initialize ofSoundPlayer.
	current_audio_ = new ofSoundPlayer();

	// Create objects which implement abstractVisualizer,
	// and add pointers to them to visualizers_.
	visualizers_[1] = new basicVizu();
	visualizers_[2] = new joyDivision();
	visualizers_[3] = new bloomWheel();

	// Use the first visualizer in the map.
	switchActiveVisualizer(1);
}

//--------------------------------------------------------------
void ofApp::update()
{
	// Move 'sliders_' to the bottom left of the screen
	sliders_.setPosition(5, ofGetWindowHeight() - 62);

	// --- FFT ANALYSIS / PROCESSING ---

	// Update vizu_data_ to reflect recent changes in audio spectrum.
	float* orig_spectrum = ofSoundGetSpectrum(kSampleBands);

	for (int i = 0; i < kVizuResolution; i++) {

		// Gradually decrease previous value (smoothing).
		long double oldValSmoothed = vizu_data_[i] * kSmoothingFactor;

		// For a value i between 0 and kVizuResolution, find an index in orig_spectrum[] 
		// to use as visualization data.
		// Since more varieties in frequency occur at lower values, it is necessary
		// to use index 'i' to refer to a cooresponding index of orig_spectrum[] through a 
		// non-linear function. This new index is stored in relSpectIdx.
		
		int relSpectIdx =
			(
				(double) (kIdxAdjustEnd - kVizuResolution)
			*	(double) pow( ( (double) i / (double) kVizuResolution ) , (double)idx_adjust_power_.value )
			+	(double) i
			);

		long double newValFromSpect = orig_spectrum[relSpectIdx];

		// The intensity of a sound is indirectly proportional to the magnitude of a 
		// given sampled frequency, which is not directly observable with our ears.
		// Graphing intensity vs frequency creates a graph which is always weighted.
		newValFromSpect = std::pow(newValFromSpect, (double)val_adjust_power_.value);


		// Use the maximum, either:
		// - the newest value of the spectrum at that index
		// - or, the last computed smoothed value.

		if (newValFromSpect > oldValSmoothed) {
			vizu_data_[i] = newValFromSpect;
		}
		else {
			vizu_data_[i] = oldValSmoothed;
		}
	}

	// --- OFSOUNDPLAYER / VISUALIZER UPDATE ---

	// Pause the music if the user has pressed the spacebar.
	// ( see keyPressed() )
	current_audio_->setPaused(paused_);
	// Update the current visualizer (IMPORTANT!)
	current_vizu_->update();
}

//--------------------------------------------------------------
void ofApp::draw() {

	// Calls the draw() function of the currently selected visualizer,
	// passing a pointer to the FFT data array (derefferenced in parameter).
	current_vizu_->draw(vizu_data_);

	// If the user's cursor is in the top right corner of the window, show
	// app info. If not, display a small icon in the corner so they know
	// where to see more information.
	if (
			ofGetMouseX() >= ofGetWindowWidth() - 30 
		&&	ofGetMouseY() <= 30) 
	{
		showAppInfo();
	}
	else {
		showInfoIcon();
	}

	// Display sliders to adjust index and value adjustment powers.
	sliders_.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
	// On 'o': Stop what's playing and open the
	// file explorer so that users can select a
	// new source audio file.
	if (key == 'o') {
		stopAudio();
		openNewSource();
	}
	// On 'p': Stop what's playing.
	if (key == 'p') {
		stopAudio();
	}
	// On 'space': Toggle the pause state of
	// what's playing.
	if (key == ' ') {
		togglePause();
	}
	// On '1'-'9': Attempt to switch the
	// current visualizer to the one at the
	// specified index.
	if (key >= 49 && key <= 57) {
		switchActiveVisualizer(key - 48);
	}
}


// --- CUSTOM METHODS ---

/*
Reset OF variables to some defaults so that one visualizer 
cannot affect the appearance of another after switching.
*/
void ofApp::resetApp() {

	ofDisableSmoothing();
	ofBackground(0, 0, 0);
}

/*
Switches the active visualizer to the one at the 
specified index pointed to in the map 'visualizers_',
if a visualizer exists there.

Also performs the setup() function of that visualizer.
*/
void ofApp::switchActiveVisualizer(int new_vizu_idx) {

	// Check if a visualizer exists at that index.
	if (visualizers_.count(new_vizu_idx)) {

		current_vizu_idx_ = new_vizu_idx;
		current_vizu_ = visualizers_.at(current_vizu_idx_);

		// Reset OpenFrameworks to default states, so that
		// visualizer performs as expected.
		resetApp();

		current_vizu_->setup();
	}
}


// -- AUDIO --

/*
If OFX is currently playing audio, fade out and stop it. If not, do nothing.
*/
void ofApp::stopAudio()
{
	if (current_audio_->isPlaying()) {
		fadeOut();
		current_audio_->stop();
	}
	return;
}

/*
Toggles the value of bool 'paused_', which takes effect when caught by 'ofApp::update()'
*/
void ofApp::togglePause()
{
	if (this->paused_) {
		this->paused_ = false;
		fadeIn();
	}
	else {
		fadeOut();
		this->paused_ = true;
	}
}

/*
Gradually increase the fade coefficient at a constant rate
so that it equals 1.0f after 0.2.
*/
void ofApp::fadeIn()
{
	// Save original fade volume (to ensure smooth fade).
	float orig_vol = current_audio_->getVolume();

	// If already at end state, escape & do nothing.
	if (orig_vol >= 1.0) {
		return;
	}
	// Increase volume to 1 in 'kFadeSteps' steps over 'kFadeLength' milliseconds.
	float fade_step_amt = ( ( 1.0f - orig_vol ) / kFadeSteps);
	for (int i = 0; i < kFadeSteps; i++)
	{
		Sleep( (float) (kFadeLength / kFadeSteps) );
		current_audio_->setVolume( i * fade_step_amt );
	}
	current_audio_->setVolume(1.0);	// Should already by set to 1.0.
									// If not, any minor offset should be eliminated.
}

/*
Gradually decrease the fade coefficient at a constant rate
so that it equals 0.0f after 0.2 seconds.
*/
void ofApp::fadeOut()
{
	// Save original fade volume (to ensure smooth fade).
	float orig_vol = current_audio_->getVolume();

	// Decrease volume to 0 in 'kFadeSteps' steps over 'kFadeLength' milliseconds.
	float fade_step_amt = ( (float) orig_vol / kFadeSteps);
	for (int i = 0; i < kFadeSteps; i++)
	{
		Sleep( (float) (kFadeLength / kFadeSteps) );
		current_audio_->setVolume( orig_vol - ( i*fade_step_amt ) );
	}
	current_audio_->setVolume(0.0);	// Should already by set to 0.0. 
									// If not, any trailing decimals will be eliminated.
}

/*
Returns whether or not FMOD is currently playing any audio.
TODO implement detection - for right now, always return false in order to trigger overlay.
*/
bool ofApp::audioPlaying() 
{
	return current_audio_->isPlaying();
}


// -- windows --

/*
Prompts the user to select an audio file on their machine 
using the native file explorer. Creates a path file from 
the input and assigns 'source_path_' to it.
*/
void ofApp::openNewSource() 
{
	ofFileDialogResult result = ofSystemLoadDialog("Load file");

	if (result.bSuccess) {
		source_path_ = result.getPath();

		current_audio_->stop();
		current_audio_->unload();
		current_audio_->load(source_path_);

		current_audio_->setVolume(0.0);
		current_audio_->play();
		fadeIn();
	}
}

/*
Accepts a pointer to an ofPolyline object, and fills the area that it
surrounds with ofColor.
*/
void ofApp::fillPolyline(ofPolyline *poly)
{
	// Create new OpenGL shape.
	ofBeginShape();
	for (int i = 0; i < (*poly).getVertices().size(); i++) {
		// Add every vertex from 'poly' to new shape.
		ofVertex( poly->getVertices().at(i).x, poly->getVertices().at(i).y);
	}
	// Render shape.
	ofEndShape();
}

/*
Accepts a pointer to an ofPolyline object, and traces lines 
between its vertices with lines of width 'thickness'.
*/
void ofApp::tracePolyline(ofPolyline *poly, int thickness) 
{
	// Initialize new mesh.
	ofMesh trace_mesh;
	trace_mesh.setMode(OF_PRIMITIVE_LINE_STRIP);
	glLineWidth(thickness);
	// Copy vertices of 'poly' into 'trace_mesh'.
	for (int i = 0; i < (*poly).getVertices().size(); i++)
	{
		ofVec3f new_vertex;
		new_vertex.x = poly->getVertices().at(i).x;
		new_vertex.y = poly->getVertices().at(i).y;

		trace_mesh.addVertex(new_vertex);
	}
	// Draw the mesh.
	trace_mesh.draw();
}

/*
informs the user of all of their available options: controls & other things.
*/
void ofApp::showAppInfo() 
{
	std::string info_full;
	
	// Adds every string from app_info_ to info_full, seperated by newlines.
	for (auto &line : app_info_) {
		info_full.append(line);
		info_full.append("\n");
	}
	// If the current visualizer contains its own description, append it to the
	// end of info_full, processed like app_info_.
	if (!current_vizu_->getDescription().empty()) {
		info_full.append("\n\n");
		
		for (auto &line : current_vizu_->getDescription()) {
			info_full.append(line);
			info_full.append("\n");
		}
	}
	// Append my personal credentials.
	info_full.append("\n---\ngithub.com/davidgm2");

	// Count the number of lines of text in 'info_full'.
	size_t info_len = std::count(info_full.begin(), info_full.end(), '\n');
	// Draw translucent rectangle as backdrop for text.
	// Make height of rectangle dependent on 'info_len'.
	ofSetColor(0, 0, 0, 150);
	ofRect(
		0,
		0,
		ofGetWindowWidth(),
		(info_len*14.1) + 20
	);
	// Draw contents of info_full ontop of backdrop.
	ofSetColor(255, 255, 255, 255);
	ofDrawBitmapString(
		info_full,
		kInfoTxtPad,
		kInfoTxtPad + 2
	);
}

/*
Display square icon in top-right corner, with letter 'i' inside.
*/
void ofApp::showInfoIcon() 
{
	// Draw translucent black square in corner.
	ofSetColor(0, 0, 0, 25);
	ofRect(ofGetWindowWidth() - kInfoButtonSize, 0, kInfoButtonSize, kInfoButtonSize);
	// Draw letter 'i' ontop of rectangle.
	ofSetColor(255, 255, 255, 255);
	ofDrawBitmapString(
		"i",
		ofGetWindowWidth() - 18,
		19
	);
}