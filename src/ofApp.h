#pragma once

#include "ofMain.h"
#include "abstractVisualizer.h"

#include "ofxGui.h"

class ofApp : public ofBaseApp {

public:

	// --- INTERFACE ---

	// Define the height and width of a square in the top
	// right corner that if a user hovers over with their
	// mouse, will display a list of options.
	const int kInfoButtonSize = 30;

	// Define the text displayed by showAppInfo().
	const std::vector<std::string> app_info_ = { 
		"Music Visualizer by David Mitchell",
		"",
		"'1-3'	switch visualizations",
		"'o'	open an .mp3, .wav, or .flac",
		"'p'	stop music",
		"' '	toggle pause"
	};

	// Define the text padding for showAppInfo() from the sides.
	const int kInfoTxtPad = 15;

	// Store ofxGUI elements
	ofxFloatSlider idx_adjust_power_;
	ofxFloatSlider val_adjust_power_;
	ofxPanel sliders_;


	// --- OFSOUNDPLAYER ---

	// Stores the path to the audio track to play.
	std::string source_path_;
	// Stores a pointer to the ofSoundPlayer object that the 
	// visualizers will refer to.
	ofSoundPlayer* current_audio_ = nullptr;
	// Stores a pointer to the current visualizer being used.
	ofAppVizu* current_vizu_;


	// --- FFT INTERPRETATION ---
	// --- Define the mathematical constants which contribute to calculating the 
	// 'index adjustment' value for FFT interpretation-- a calculation which
	// ensures that our visualizers do not display frequency VS intensity as a linear
	// function of frequency, rather an exponential one, to better examine the range
	// of frequencies that humans pay attention to most, particularly in music.

	// A graph of this calculation can be found at: 
	// https://www.desmos.com/calculator/1whq5ji1da
	// i = kVizuResolution
	// j = kIdxAdjustEnd
	// a = kIdxAdjustPower

	// Define the length of the float array to be returned by ofSoundGetSpectrum().
	// 1 <= kSampleBands <= 512
	const int kSampleBands = 512;

	// Define how many indexes of the ofSoundGetSpectrum() float array will
	// be accessed from kSampleStart and kSampleEnd.
	// 1 <= kVizuResolution <= kSampleBands
	static constexpr size_t kVizuResolution = 128;

	// Store a float array of size 'kVizuResolution' to represent adjusted FFT data.
	std::array<double, 128> vizu_data_;

	// Define the factor which each value in vizu_data_ will be multiplied with when
	// no peaks have been recently recorded - this defines the rate for "smoothing".
	// 0.0 < kSmoothingFactor < 1.0, but ideally: 0.9 < kSmoothingFactor < 1.0
	const double kSmoothingFactor = 0.98;

	// Define the ending index of the ofSoundSpectrum() array to analyze.
	// kVizuResolution <= kIdxAdjustEnd <= kSampleBands
	const int kIdxAdjustEnd = 512;
	// Define the power of the function drawing a curve between 
	// (0, 0) and (kVizuResolution, kSampleEnd)
	const double kIdxAdjustPower = 10.0;

	// Define the power to adjust the values to- so that variations at lower intensities
	// are easier to see and regular peaks do not overshadow the rest of the motion.
	// 0 <= kValAdjustPower <= 1
	const double kValAdjustPower = 0.4;


	// --- VISUALIZATION ---
	
	// Store a map of pointers to objects which implement abstractVisualizer.
	// Will be accessed through numkeys 1-9, so do not add more than 9 at a time.
	std::map<int, ofAppVizu*> visualizers_;
	// Keep track of the current visualizer to display.
	int current_vizu_idx_;


	// --- PAUSE/PLAY PARAMETERS ---

	// True if the music player is paused, false if not.
	bool paused_;

	// The length of time that fadeIn() and fadeOut() should run in, in milliseconds.
	const int kFadeLength = 200;
	// The amount of steps taken by fadeIn() and fadeOut() while adjusting vol_fade_coef_.
	const int kFadeSteps = 10;


	// --- DEFAULT OFX METHODS ---

		void setup();
		void update();
		void draw();

		void keyPressed(int key);


	// --- CUSTOM METHODS ---

		// OpenFrameworks
		void resetApp();
		void switchActiveVisualizer(int new_vizu_idx);
		static void fillPolyline(ofPolyline *poly);
		static void tracePolyline(ofPolyline *poly, int thickness);

		// ofSound
		void stopAudio();
		void togglePause();
		void fadeIn();
		void fadeOut();

		bool audioPlaying();

		// Windows
		void openNewSource();

		// Interface
		void showAppInfo();
		void showInfoIcon();
};
