#ifndef _OFAPPVIZU
#define _OFAPPVIZU

#include "ofApp.h"

class ofAppVizu {

protected:
	std::array<float, 128> *fft_spect_ = nullptr;

public:
	virtual void setup() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
};
#endif